# Affecter IDs

![icon_affector](./obstacle_ids/icon_affector.png)

## Wind

![wind](./obstacle_ids/wind.png)

* 154 - default
* 155 - stronger
* 156 - even stronger

## Slime Block

![slime_block](./obstacle_ids/slime_block.png)

* 116 - default
* 117 - bigger
* 118 - stronger
* 147 - even bigger
* 148 - even stronger

## Hoverzone

![hoverzone](./obstacle_ids/hoverzone.png)

* 171 - default
* 172 - bigger
* 173 - even bigger

## Non Jump

![non_jump](./obstacle_ids/non_jump.png)

* 113 - default
* 114 - bigger
* 115 - even bigger
* 157 - extendable field

## Magnet

![magnet](./obstacle_ids/magnet.png)

* 159 - (pull) default 
* 160 - (pull) switches on and off
* 162 - (pull) switches on and off, randomly
* 163 - (push) default
* 164 - (push) switches on and off
* 165 - (push) switches on and off, randomly
* 166 - magnet switches between push and pull
* 167 - magnet switches between push and pull, randomly