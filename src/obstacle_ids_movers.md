# Mover IDs

![icon_movers](./obstacle_ids/icon_movers.png)

## Moving Platform

![moving_platform](./obstacle_ids/moving_platform.png)

* 140 - default, moves forwards/backwards
* 141 - moves further
* 142 - moves even further
* 143 - moves faster
* 144 - moves even faster
* 177 - moves in the shape of a triangle
* 178 - moves in the shape of a cube

## Windmill

![windmill](./obstacle_ids/windmill.png)

* 41 - default
* 42 - longer wings
* 43 - even longer wings
* 44 - thicker windmill
* 45 - even thicker windmill
* 46 - a faster windmill
* 47 - more faster windmill
* 48 - the fastest windmill
* 49 - spinner windmill
* 50 - spinner windmill a bit bigger
* 51 - spinner windmill bigger, slightly faster
* 52 - spinner windmill even bigger

## Moving Wall

![moving_wall](./obstacle_ids/moving_wall.png)

* 53 - A wall which moves from side to side
* 54 - moves faster
* 55 - moves even faster
* 56 - doubled wall
* 57 - doubled wall a bit bigger
* 58 - doubled wall even bigger
* 59 - quadrupled moving wall
* 60 - quadrupled moving wall, bigger/thicker

## Sponner

![sponner](./obstacle_ids/sponner.png)

* 130 - default
* 131 - cross shape
* 132 - thicker/taller shape
* 133 - offset shape
* 134 - crazy offset shape

## Bumper Plate

![bumper_plate](./obstacle_ids/bumper_plate.png)

* 135 - cubes randomly bumping out of a huge wall
