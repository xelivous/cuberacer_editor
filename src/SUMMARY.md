# Summary

- [Overview](./overview.md)
- [Obstacle IDs]()
    - [Normal IDs](./obstacle_ids_normal.md)
    - [Mover IDs](./obstacle_ids_movers.md)
    - [Deadly IDs](./obstacle_ids_deadly.md)
    - [Booster IDs](./obstacle_ids_booster.md)
    - [Affecter IDs](./obstacle_ids_affecter.md)
    - [Special IDs](./obstacle_ids_special.md)