# Booster IDs

![icon_booster](./obstacle_ids/icon_booster.png)

## Booster

![booster](./obstacle_ids/booster.png)

* 97 - default
* 98 - a stronger boost
* 99 - an even stronger boost
* 100 - a bigger boost field
* 153 - a bigger boost field, with a stronger boost

## Jumppad

![jumppad](./obstacle_ids/jumppad.png)

* 101 - default
* 102 - stronger jump power
* 103 - even stronger jump power
* 104 - bigger jump field
* 105 - even bigger jump field

## Canon (cannon)

![canon](./obstacle_ids/canon.png)

* 106 - default
* 107 - stronger
* 108 - even stronger
* 109 - SUPER STRONG
* 110 - bigger opening
* 111 - even bigger opening

## Portal

![portal](./obstacle_ids/portal.png)

* 112 - default
* 158 - Door portal

## Pipe Straight

![pipe_straight](./obstacle_ids/pipe_straight.png)

* 121 - default
* 149 - more succ

## Pipe Angle

![pipe_angle](./obstacle_ids/pipe_angle.png)

* 122 - default

## Funnel

![funnel](./obstacle_ids/funnel.png)

* 123 - default
* 124 - bigger
* 125 - even bigger
