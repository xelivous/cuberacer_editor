# Deadly IDs

![icon_deadly](./obstacle_ids/icon_deadly.png)

## Spikes

![spikes](./obstacle_ids/spikes.png)

* 63 - default
* 64 - bigger spikefield
* 65 - Even bigger spikefield
* 129 - Even much bigger spikefield

## Deadly Rotator

![deadly_rotator](./obstacle_ids/deadly_rotator.png)

* 73 - default
* 74 - A longer rotator
* 75 - A bigger rotator
* 76 - An even bigger rotator
* 136 - A loooonger rotator
* 137 - A more complex rotator
* 138 - An even more complex rotator

## Deadly Spinner

![deadly_spinner](./obstacle_ids/deadly_spinner.png)

* 92 - default
* 93 - spikes on all sides
* 94 - spikes on all sides, and longer
* 95 - longer
* 96 - longer and taller

## Rail Rotator

![rail_rotator](./obstacle_ids/rail_rotator.png)

* 80 - default
* 81 - a wider rotator
* 82 - an even wider rotator
* 83 - rotating around the rail
* 84 - rotating around the rail, longer spike

## Saw

![saw](./obstacle_ids/saw.png)

* 85 - default
* 86 - this saw drives faster from place to place
* 87 - a bigger saw blade
* 88 - an even bigger saw blade
* 89 - this saw drives in a crazy pattern
* 90 - this saw drives in a crazy pattern (faster)
* 127 - this saw is even deadly on the blade

## Death Grid

![death_grid](./obstacle_ids/death_grid.png)

* 66 - default
* 67 - a little bigger
* 68 - 4 square
* 69 - 4 square bigger
* 70 - 9 square
* 71 - special formed
* 72 - more special formed

## Hammer

![hammer](./obstacle_ids/hammer.png)

* 77 - Hits a random location
* 78 - Hitting on one spot with a huge area
* 79 - Hits from side to side

## Swinging Axe

![swinging_axe](./obstacle_ids/swinging_axe.png)

* 91 - default
* 168 - +rotating

## Button Trap

![button_trap](./obstacle_ids/button_trap.png)

* 126 - default
* 128 - a bigger button, that can be hidden underground
* 150 - bigger button, bigger trap
* 151 - bigger, bigger, bigger
* 152 - the trap snaps if a player jumps over it