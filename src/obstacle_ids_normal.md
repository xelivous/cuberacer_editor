# Obstacle IDs

![icon_normal](./obstacle_ids/icon_normal.png)

## Normal Wall

![normal_wall](./obstacle_ids/normal_wall.png)

* 6 - default
* 7 - bigger
* 8 - even bigger
* 9 - the biggest
* 10 - Free scaleable wall
* 11 - a thicker wall
* 12 - a even thicker wall

## Gap Wall

![gap_wall](./obstacle_ids/gap_wall.png)

* 13 - default
* 14 - bigger
* 15 - more depth
* 16 - single passage
* 17 - single hole
* 18 - double hole
* 19 - tunnel gap
* 20 - big tunnel gap

## Tilted Platform 

![tilted_platforms](./obstacle_ids/tilted_platforms.png)

* 0 - default
* 1 - A little bigger
* 2 - Still a little bigger
* 3 - A little longer
* 4 - Still a little longer
* 5 - Has a variable tilt

## Block

![block](./obstacle_ids/block.png)

* 21 - A simple block
* 22 - A smaller block
* 23 - A even smaller block
* 24 - A block with crazy expansions
* 25 - A block with some crazy expansions. Some deadly.
* 26 - A block with some crazy expansions. All deadly.
* 145 - block with rounded corners
* 146 - block with rounded corners (larger)

## Sieve

![sieve](./obstacle_ids/sieve.png)

* 27 - small with slats
* 28 - large with slats
* 29 - small with tiny squares
* 30 - large with tiny squares

## Long Dong

![long_dong](./obstacle_ids/long_dong.png)

* 31 - default
* 32 - longer
* 33 - wider

## Waves

![waves](./obstacle_ids/waves.png)

* 34 - default
* 35 - longer
* 36 - longer and wider
* 37 - less frequent
* 38 - less frequent and wider
* 39 - less frequent, wider, and longer

## Glass Wall

![glass_wall](./obstacle_ids/glass_wall.png)

* 174 - default
* 175 - bigger
* 176 - cubic
