# Special IDs

![icon_special](./obstacle_ids/icon_special.png)

## Radio

![radio](./obstacle_ids/radio.png)

* 40 - A music playing radio
* 169 - New songs new power

## Duster

![duster](./obstacle_ids/duster.png)

* 61 - Spits dust above
* 62 - Spits dust to the side
* 179 - Spits dust in circles

## Wrecking Ball

![wrecking_ball](./obstacle_ids/wrecking_ball.png)

* 139 - massive wrecking ball
* 161 - golden wrecking ball with some more power
* 170 - super tiny wrecking ball, with decent power.

## Black Hole

![black_hole](./obstacle_ids/black_hole.png)

* 119 - pulls and kills on touch (black)
* 120 - pulls through and doesn't kill (white)
